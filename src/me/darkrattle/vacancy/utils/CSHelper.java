package me.darkrattle.vacancy.utils;

import me.darkrattle.vacancy.VACancy;
import me.darkrattle.vacancy.engine.Player;

public class CSHelper {

	private static int m_dwEntityList = 0x04A9E714;
	private static int m_dwClientState = 0x00610344;
	private static int m_dwLocalPlayer = 0x00A834DC;

	private static int m_dwInGame = 0x00000100;
	private static int EntitySize = 0x10;

	private static int client;
	private static int engine;

	private static Player localPlayer;

	public CSHelper() {
		client = VACancy.getGame().getBaseAddress(VACancy.getGame().getProcess(), "client.dll");
		engine = VACancy.getGame().getBaseAddress(VACancy.getGame().getProcess(), "engine.dll");
	}

	public static void update() {
		localPlayer = new Player(VACancy.getGame().readMemory(CSHelper.getClient() + m_dwLocalPlayer, 4).getInt(0));

	}

	public static boolean isLocalPlayerIngame() {
		int enginePointer = VACancy.getGame().readMemory(engine + m_dwClientState, 4).getInt(0);
		int inGame = VACancy.getGame().readMemory(enginePointer + m_dwInGame, 4).getInt(0);

		return inGame == 6;
	}

	public static int getPlayerAddress(int playerID) {
		return VACancy.getGame().readMemory(client + m_dwEntityList + (EntitySize * playerID), 4).getInt(0);
	}

	public static byte[] getBytesFromFloat(Float number) {
		return new byte[] { (byte) (Float.floatToIntBits(number) & 0xFF), (byte) ((Float.floatToIntBits(number) >> 8) & 0xFF), (byte) ((Float.floatToIntBits(number) >> 16) & 0xFF), (byte) ((Float.floatToIntBits(number) >> 24) & 0xFF) };
	}

	public static int getClient() {
		return client;
	}

	public static int getEngine() {
		return engine;
	}

	public static Player getLocalPlayer() {
		return localPlayer;
	}

}
