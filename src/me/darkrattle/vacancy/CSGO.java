package me.darkrattle.vacancy;

import javax.swing.JOptionPane;

import me.darkrattle.vacancy.engine.hack.Hack;
import me.darkrattle.vacancy.engine.hack.HackManager;
import me.darkrattle.vacancy.utils.CSHelper;
import me.darkrattle.vacation.engine.Game;

public class CSGO extends Game {

	public CSGO() {
		super("Counter-Strike: Global Offensive");
	}

	@Override
	public void run() {
		while (isGameRunning()) {
			try {
				Thread.sleep(1);
				
				CSHelper.update();

				for (Hack hack : HackManager.getHacks()) {
					if (hack.isEnabled()) {
						hack.onEnable();
					}
				}
			} catch (Exception exception) {
				exception.printStackTrace();
			}
		}

		JOptionPane.showMessageDialog(null, getName() + " not found, please run the game.", "Process not found.", JOptionPane.ERROR_MESSAGE);
		System.exit(0);
	}

}
