package me.darkrattle.vacancy.ui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;

import me.darkrattle.vacancy.VACancy;
import me.darkrattle.vacancy.engine.hack.HackManager;
import me.darkrattle.vacancy.hacks.Bhop;
import me.darkrattle.vacancy.hacks.NoFlash;
import me.darkrattle.vacancy.hacks.Triggerbot;

public class Window extends JFrame {

	private static final long serialVersionUID = 1L;

	private JPanel contentPane;
	private JTextField txtTriggerbot;
	private JTextField txtAimbot;
	private JTextField textFieldBhop;
	private JTextField textFieldEsp;
	private JTextField textFieldNoFlash;

	public Window() {

		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception exception) {
			exception.printStackTrace();
		}

		setTitle(VACancy.NAME + " " + VACancy.VERSION);
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 299, 265);
		setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(10, 11, 273, 214);
		contentPane.add(tabbedPane);

		JPanel panel = new JPanel();
		tabbedPane.addTab("Main", null, panel, null);
		panel.setLayout(null);

		JLabel lblTriggerbot = new JLabel("Triggerbot:");
		lblTriggerbot.setBounds(10, 119, 60, 14);
		panel.add(lblTriggerbot);

		txtTriggerbot = new JTextField();
		txtTriggerbot.setText("OFF");
		txtTriggerbot.setBounds(208, 110, 50, 30);
		panel.add(txtTriggerbot);
		txtTriggerbot.setColumns(10);

		JButton btnTriggerbotToggle = new JButton("Toggle");
		btnTriggerbotToggle.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				toggleTriggerbot();
			}
		});
		btnTriggerbotToggle.setBounds(80, 110, 118, 30);
		panel.add(btnTriggerbotToggle);

		JLabel lblAimbot = new JLabel("Aimbot:");
		lblAimbot.setBounds(10, 19, 60, 14);
		panel.add(lblAimbot);

		JButton btnAimbotToggle = new JButton("Toggle");
		btnAimbotToggle.setBounds(80, 11, 118, 30);
		panel.add(btnAimbotToggle);

		txtAimbot = new JTextField();
		txtAimbot.setText("OFF");
		txtAimbot.setBounds(208, 11, 50, 30);
		panel.add(txtAimbot);
		txtAimbot.setColumns(10);

		JLabel labelBhop = new JLabel("Bhop:");
		labelBhop.setBounds(10, 52, 60, 14);
		panel.add(labelBhop);

		JButton buttonBhopToggle = new JButton("Toggle");
		buttonBhopToggle.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				toggleBhop();
			}
		});
		buttonBhopToggle.setBounds(80, 44, 118, 30);
		panel.add(buttonBhopToggle);

		textFieldBhop = new JTextField();
		textFieldBhop.setText("OFF");
		textFieldBhop.setColumns(10);
		textFieldBhop.setBounds(208, 44, 50, 30);
		panel.add(textFieldBhop);

		JLabel labelEsp = new JLabel("ESP:");
		labelEsp.setBounds(10, 85, 60, 14);
		panel.add(labelEsp);

		JButton buttonToggleEsp = new JButton("Toggle");
		buttonToggleEsp.setBounds(80, 77, 118, 30);
		panel.add(buttonToggleEsp);

		textFieldEsp = new JTextField();
		textFieldEsp.setText("OFF");
		textFieldEsp.setColumns(10);
		textFieldEsp.setBounds(208, 77, 50, 30);
		panel.add(textFieldEsp);
		
		JLabel lblNoFlash = new JLabel("NoFlash:");
		lblNoFlash.setBounds(10, 153, 60, 14);
		panel.add(lblNoFlash);
		
		JButton buttonNoFlash = new JButton("Toggle");
		buttonNoFlash.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				toggleNoFlash();
			}
		});
		buttonNoFlash.setBounds(80, 144, 118, 30);
		panel.add(buttonNoFlash);
		
		textFieldNoFlash = new JTextField();
		textFieldNoFlash.setText("OFF");
		textFieldNoFlash.setColumns(10);
		textFieldNoFlash.setBounds(208, 144, 50, 30);
		panel.add(textFieldNoFlash);
	}

	public void toggleTriggerbot() {
		HackManager.getHack(Triggerbot.class).toggle();

		if (HackManager.getHack(Triggerbot.class).isEnabled()) {
			txtTriggerbot.setText("ON");
		} else {
			txtTriggerbot.setText("OFF");
		}
	}

	private void toggleBhop() {
		HackManager.getHack(Bhop.class).toggle();

		if (HackManager.getHack(Bhop.class).isEnabled()) {
			textFieldBhop.setText("ON");
		} else {
			textFieldBhop.setText("OFF");
		}
	}
	
	private void toggleNoFlash() {
		HackManager.getHack(NoFlash.class).toggle();

		if (HackManager.getHack(NoFlash.class).isEnabled()) {
			textFieldNoFlash.setText("ON");
		} else {
			textFieldNoFlash.setText("OFF");
		}
	}

}
