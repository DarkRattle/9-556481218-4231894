package me.darkrattle.vacancy;

import java.awt.EventQueue;

import me.darkrattle.vacancy.ui.Window;
import me.darkrattle.vacancy.utils.CSHelper;
import me.darkrattle.vacation.engine.Game;

public class VACancy {

	public static final String NAME = "VACancy";
	public static final String VERSION = "v1.0";
	public static final String[] AUTHORS = { "DarkRattle" };

	private static Game game;

	public VACancy() {
		getGame().start();
		new CSHelper();
	}

	public static Game getGame() {
		if (game == null) {
			game = new CSGO();
		}
		return game;
	}

	public static void main(String[] args) {
		new VACancy();
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Window frame = new Window();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}


}
