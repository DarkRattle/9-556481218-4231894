package me.darkrattle.vacancy.hacks;

import me.darkrattle.vacancy.engine.Player;
import me.darkrattle.vacancy.engine.hack.Hack;
import me.darkrattle.vacancy.utils.CSHelper;

public class Triggerbot extends Hack {

	private int m_dwForceAttack = 0x02EDE710;

	public Triggerbot() {
		super("Triggerbot");
	}

	@Override
	public void onEnable() {
		try {
			if (!CSHelper.isLocalPlayerIngame()) {
				return;
			}

			int playerID = CSHelper.getLocalPlayer().getCrosshairID();

			Player enemy = new Player(CSHelper.getPlayerAddress(playerID));

			if (playerID < 64 && playerID >= 0 && (enemy.getTeam() != CSHelper.getLocalPlayer().getTeam()) && !enemy.isDead()) {
				attack();
			}

		} catch (Exception exception) {
			exception.printStackTrace();
		}
	}

	private void attack() {
		try {
			getGame().writeMemory(CSHelper.getClient() + m_dwForceAttack, new byte[] { 5 });
			Thread.sleep(15);
			getGame().writeMemory(CSHelper.getClient() + m_dwForceAttack, new byte[] { 4 });
		} catch (Exception exception) {
			exception.printStackTrace();
		}
	}

}
