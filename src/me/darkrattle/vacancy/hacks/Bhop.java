package me.darkrattle.vacancy.hacks;

import org.jnativehook.keyboard.NativeKeyEvent;

import me.darkrattle.vacancy.engine.hack.Hack;
import me.darkrattle.vacancy.utils.CSHelper;

public class Bhop extends Hack {

	private int m_dwForceJump = 0x04F33AA8;

	public Bhop() {
		super("Bhop");
	}

	@Override
	public void onEnable() {
		try {
			if (!CSHelper.isLocalPlayerIngame()) {
				return;
			}

			if (getGame().getKeyInput().isKeyPressed(NativeKeyEvent.VC_SPACE) && canJump()) {
				jump();
			}

		} catch (Exception exception) {
			exception.printStackTrace();
		}
	}

	private void jump() {
		try {
			getGame().writeMemory(CSHelper.getClient() + m_dwForceJump, new byte[] { 5 });
			Thread.sleep(50);
			getGame().writeMemory(CSHelper.getClient() + m_dwForceJump, new byte[] { 4 });
		} catch (Exception exception) {
			exception.printStackTrace();
		}
	}

	private boolean canJump() {
		int mFlags = getGame().readMemory(CSHelper.getLocalPlayer().getAddress() + 0x100, 4).getInt(0);
		return mFlags == 257;
	}

}
