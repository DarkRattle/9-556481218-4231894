package me.darkrattle.vacancy.hacks;

import me.darkrattle.vacancy.engine.hack.Hack;
import me.darkrattle.vacancy.utils.CSHelper;

public class NoFlash extends Hack {

	private int m_flFlashDuration = 0x0000A2F8;

	public NoFlash() {
		super("NoFlash");
	}

	@Override
	public void onEnable() {
		try {
			if (!CSHelper.isLocalPlayerIngame()) {
				return;
			}

			if (isPlayerFlashed()) {
				System.out.println("FLASHED");
				getGame().writeMemory(CSHelper.getLocalPlayer().getAddress() + m_flFlashDuration, CSHelper.getBytesFromFloat(0f));
			}

		} catch (Exception exception) {
			exception.printStackTrace();
		}
	}

	private boolean isPlayerFlashed() {
		return getGame().readMemory(CSHelper.getLocalPlayer().getAddress() + m_flFlashDuration, 4).getInt(0) > 0;
	}
	
}
