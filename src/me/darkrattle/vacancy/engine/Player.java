package me.darkrattle.vacancy.engine;

import me.darkrattle.vacancy.VACancy;
import me.darkrattle.vacation.engine.Game;

public class Player {

	private int m_lifeState = 0x0000025B;
	private int m_iTeamNum = 0x000000F0;
	private int m_iCrossHairID = 0x0000AA44;

	private Game game;
	private int address;

	public Player(int address) {
		this.game = VACancy.getGame();
		this.address = address;
	}

	public boolean isDead() {
		return game.readMemory(address + m_lifeState, 4).getInt(0) != 0;
	}

	public int getTeam() {
		return game.readMemory(address + m_iTeamNum, 4).getInt(0);
	}

	public int getCrosshairID() {
		return game.readMemory(address + m_iCrossHairID, 4).getInt(0) - 1;
	}

	public Game getGame() {
		return game;
	}

	public int getAddress() {
		return address;
	}

}
