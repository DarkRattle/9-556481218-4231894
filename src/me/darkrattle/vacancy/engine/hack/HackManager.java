package me.darkrattle.vacancy.engine.hack;

import java.util.Arrays;
import java.util.List;

import me.darkrattle.vacancy.hacks.Bhop;
import me.darkrattle.vacancy.hacks.NoFlash;
import me.darkrattle.vacancy.hacks.Triggerbot;

public class HackManager {

	private static List<Hack> hacks = Arrays.asList(new Bhop(), new Triggerbot(), new NoFlash());

	public static Hack getHack(Class<? extends Hack> clazz) {
		for (Hack hack : hacks) {
			if (hack.getClass() == clazz) {
				return hack;
			}
		}

		return null;
	}

	public static List<Hack> getHacks() {
		return hacks;
	}

}
