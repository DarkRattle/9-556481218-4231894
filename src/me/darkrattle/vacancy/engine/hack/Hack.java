package me.darkrattle.vacancy.engine.hack;

import me.darkrattle.vacancy.VACancy;
import me.darkrattle.vacation.engine.Game;

public abstract class Hack {

	private String name;
	private Game game;

	private boolean enabled;

	public Hack(String name) {
		this.name = name;
		this.game = VACancy.getGame();
	}

	public void toggle() {
		if (enabled) {
			enabled = false;
		} else {
			enabled = true;
		}
	}

	public void setState(boolean state) {
		enabled = state;
	}

	public abstract void onEnable();

	public boolean isEnabled() {
		return enabled;
	}

	public String getName() {
		return name;
	}

	public Game getGame() {
		return game;
	}

}
